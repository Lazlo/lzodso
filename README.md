# lzoDSO

A digital storage oscilloscope (DSO) built as open-source hardware.

## Repository Contents

```
keypad/		Firmware for keypad
meta-lzodso/	Yocto layers
sim/		Simulator for software development
```

## Checkout

```
git clone git@gitlab.com:Lazlo/lzodso.git
cd lzodso
git submodule update --init --recursive
```
